package com.stock.rest;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stock.domain.StockAccount;
import com.stock.service.StockAccountService;

//TODO Declare the class is a REST Controller
//TODO Map the entire class to the /accounts URI
@RestController
@RequestMapping("/accounts")
public class StockAccountResource {

	//TODO Inject the stock account service
	@Autowired
	StockAccountService stockAccount;
	

	//TODO Create a method to retrieve all the stock accounts
	@RequestMapping
	public Collection<StockAccount> getAllStocks() {
		return stockAccount.findAll();
	}
	
	//TODO Create a method to retrieve a stock account by id
	// https://stackoverflow.com/questions/35155916/handling-ambiguous-handler-methods-mapped-in-rest-application-with-spring
	 @RequestMapping("/id/{id}")
	 public StockAccount findStockAccountById(@PathVariable("id")Long id) {
		return stockAccount.findById(id);
	 }
	
	//TODO Create a method to retrieve a stock account by name
	 @RequestMapping("/name/{name}")
	 public Collection<StockAccount> findStockAccountByName(@PathVariable("name")String name) {
		 return stockAccount.findByName(name);
	 }

}
